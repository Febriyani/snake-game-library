#include <iostream>
//#include "Windows.h"
#include "GameRules.h"
#include <Windows.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

GameRules::GameRules() {
	arena.draw();
	player.draw();
	obstacle.randPos();
	obstacle.Draw();
}

void GameRules::move() {
	//fungsi yang mengatur pergerakan player
	char input = 'l';
	char lastInput = 'w';
	//player.moveLeft();

	while (true) {

		if (_kbhit()) {
			input = _getch();
			
			switch (input) {
			case 'a':
				lastInput = 'a';
				break;

			case 'd':
				lastInput = 'd';
				break;

			case 'w':
				lastInput = 'w';
				break;

			case 's':
				lastInput = 's';
				break;
				break;
			default:
				break;
			}
		}

		switch (lastInput) {
			case 'a':
				player.clear();
				player.moveLeft();
				player.draw();
				break;

			case 'd':
				player.clear();
				player.moveRight();
				player.draw();
				break;

			case 'w':
				player.clear();
				player.moveUp();
				player.draw();
				break;

			case 's':
				player.clear();
				player.moveDown();
				player.draw();
				break;
			default:
				break;
		}
		//while (input != NULL)
		//player.keepMoving(input);
		colFruit();
		colWall();
		colBody();

		Sleep(150);
	}
}

//fungsi yang akan dijalankan ketika player menabrak obstacle/fruit
void GameRules::colFruit()
{
	//jika player berada dalam satu posisi yang sma dengan obstacle fungsi ini akan dijalankan
	if (obstacle.position.x == player.x() && obstacle.position.y == player.y()) {
		//system("pause");
		obstacle.randPos();//pemaggilan fungsi randPos pada class obstacle
		obstacle.Draw();//pemaggilan fungsi Draw pada class obstacle
		player.addTail();//pemaggilan fungsi addTail pada class player
	}
}

//fungsi yang akan dijalankan ketika player menabrak wall/dinding
void GameRules::colWall()
{
	//jika player berada pada dinding layar permainan maka fungsi ini akan dijalankan
	if (player.x() <= 0 || player.x() >= 84 - 2 || player.y() <= 0 || player.y() >= 20 - 1) {
		gameOver();
	}
}

//fungsi yag akan dijalankan ketika kepala dan tubuh snake bertabrakan
void GameRules::colBody()
{
	bool bodycollision = false; 
	int head = 0;

	for (Object object : player.snake) {
		Position bodyPos = object.position;
		Position headPos = player.snake.front().position;

		if (head < 2) {
			head++;
			continue; 
		}else if (headPos.x == bodyPos.x && headPos.y == bodyPos.y) {
			gameOver();
			break;
			
		}

		head++;
	}

}

//fungsi yag akan mengatur kondisi kalah
void GameRules::gameOver()
{
	Position gameOverPos;

	gameOverPos.gotoxy(74 / 2, 18 / 2);
	cout << " GAME is OVER ";
	gameOverPos.gotoxy( 60 / 2, 20 / 2);
	system("pause");
}
