#include <iostream>
#include "Object.h"
#include "Player.h"
#include "Obstacle.h"
// #include "Windows.h"

using namespace std;

Player::Player()
{
	Object object; //memanggil class object
	object.position.x = 15;//posisi player awal mula permainan
	object.position.y = 10;
	object.skin = '@';

	snake.push_back(object);

	object.position.x = 16;
	snake.push_back(object);//berfungsi untuk update char
	object.position.x = 17;
	snake.push_back(object);
	object.position.x = 18;
	snake.push_back(object);
}

int Player::x()
{
	Position headpos = snake.front().position; //menentukan posisi kepala
	return headpos.x;
}

int Player::y()
{
	Position headpos = snake.front().position; //menentukan posisi kepala
	return headpos.y;
}

//memunculkan object player da score
void Player::draw()
{
	for (Object object : snake)//memunculkan object player
	{
		Position pos = object.position;
		pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		cout << object.skin;
		pos.gotoxy(0, 21);
	}

	Position scorePos; //menampilkan score 
	scorePos.gotoxy(0, 21);
	cout << "Your Score : " << score << endl;
}

//unuk menghapus posisi sebelum terjadi update char
void Player::clear()
{
	for (Object object : snake)
	{
		Position pos = object.position;
		pos.gotoxy(pos.x, pos.y);
		cout << " ";
		pos.gotoxy(0, 21);
	}
}

//pergerakan player
void Player::keepMoving(char i)
{
	switch(i) {
		case 'a':
			moveLeft();
			break;

		case 'd':
			moveRight();
			break;

		case 'w':
			moveUp();
			break;

		case 's':
			moveDown();
			break;
		default:
			break;
	}
}

//identifaksi posisi tubuh snake
int Player::bodyPosX()
{
	Position pos;
	for (Object object : snake)
	{
		pos = object.position;
		//pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		//cout << object.skin;
		//pos.gotoxy(0, 21);
	}
	
	return pos.x;
}

//identifaksi posisi tubuh snake
int Player::bodyPosY()
{
	Position pos;
	for (Object object : snake)
	{
		pos = object.position;
		//pos.gotoxy(pos.x, pos.y);
		//gotoxy(pos.x, pos.y);
		//cout << object.skin;
		//pos.gotoxy(0, 21);
	}

	return pos.y;
}

//ekor dan skor akan bertambah jika menabrak obstacle 
void Player::addTail()
{
	Obstacle obs;
	Position newTail = obs.position;

	Position tailPos = snake.back().position;

	Object tail;
	tail.position = tailPos;
	snake.push_back(tail);
	
	score++;

}

//pergerakan snake ke atas
void Player::moveUp()
{
	Position headPos = snake.front().position;
	headPos.y--;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

//pergerakan snakee bawah
void Player::moveDown()
{
	Position headPos = snake.front().position;
	headPos.y++;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

//pergerakan snake kekiri
void Player::moveLeft()
{
	Position headPos = snake.front().position;
	headPos.x--;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}

//pergerakan snake kekkiri
void Player::moveRight()
{
	Position headPos = snake.front().position;
	headPos.x++;

	Object last = snake.back();
	last.position = headPos;
	snake.pop_back();
	snake.push_front(last);
}