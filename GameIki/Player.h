#pragma once
#include <list>
#include "Object.h"

using namespace std;

//inisialisasi variabel dan fungsi
class Player {
private:
	
public:
	Player();
	list<Object> snake;
	int x();
	int y();
	int score = 0; 

	void draw();
	void clear();

	void keepMoving(char i);
	int bodyPosX();
	int bodyPosY();
	void addTail();

	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
};