#pragma once
#include "Player.h"

class Board {
	//class ini digunakan untuk membuat bentuk suatu arena permainan
private:
	char maps[20][84] = {
		"###################################################################################",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"#                                                                                 #",
		"###################################################################################"
	};

	Player player;

public:
	//sebuah fungsi yang akan dipanggil jika akan menggambar arena permainan
	void draw();
};