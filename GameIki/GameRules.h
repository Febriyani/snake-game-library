#pragma once
#include "Board.h"
#include "Player.h"
#include "Obstacle.h"

class GameRules {
private:
	Board arena; //berfungsi untuk memanggil class board
	Player player; //berfungsi untuk memanggil class player
	Obstacle obstacle; //berfungsi untuk memanggil class obstacle

public:
	//fungsi yang ada didalam class GameRules
	GameRules();
	//inisialisasi dari fungsi yang ada pada class GameRules.cpp
	void move(); 
	void colFruit();
	void colWall();
	void colBody();
	void gameOver();

};