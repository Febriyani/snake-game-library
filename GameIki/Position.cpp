#include "Position.h"
#include <iostream>
#include <Windows.h>

//untuk mengatur posisi console/cursor
HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
COORD CursorPosition;

//fungsi yang mengatur posisi cursor
void Position::gotoxy(int x, int y)
{
	CursorPosition.X = x;
	CursorPosition.Y = y;
	SetConsoleCursorPosition(console, CursorPosition);
}